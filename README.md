## Búsqueda Heurística para identificar población Millennials
Se plantean las definiciones y las respectivas restricciones para generar una búsqueda que optimice a qué Millennials se debe ofrecer determinado producto bancario según su conducta de pago y relación de gasto mensual.

---

** Situación del Problema
Se requiere optimizar búsqueda de persona Millennials según su definición y conducta de gasto mensual sugerir qué portafolio bancario se puede ofertar considerando que puede tener uno o más productos bancarios activos, y que cada
producto tiene condiciones de % de sueldo disponible para considerar que el Millennials tiene capacidad de pago.

---

## Lenguaje de Restricciones
Regla 1 = Una persona es Millennials si ha nacido entre los años 1980 a 2000
Regla 2 = Una persona es Millennials si ha tenido 4 empleos distintos en los últimos 2 años
Regla 3 = Una persona debe tener por lo menos un tipo de cuenta vista, rut, corriente o ahorro con un saldo de cuenta disponible que le permite pagar un producto de crédito bancario
Regla 4 = Una persona puede tener uno o más productos de créditos activos 
Regla 5 = Una persona que tienen un producto de crédito activo con el banco debe tener un pago a tiempo según la liquidación de éste. Debe pagar % de interés sin morosidad y a tiempo
Regla 5 = Una persona es Millennials tiene formación universitaria y sueldo es mayor al promedio de la población
Regla 6 = Una persona Millennials tiene capacidad de pago para vivir en comunas como Vitacura, Las Condes, La Reina o ñuñoa
Regla 7 = Opciones de crédito 0.7 para crédito hipotecario de sueldo libre; 0,4 automóvil; 0,3 consumo y 0,2 tarjeta de crédito
Regla 8 = Una persona Millennials puede optar por un fondo de ahorro siempre que sueldo sea mayor a la relación de gastos mensuales y tenga un monto de ahorro disponible.

---

** Función Objetivo
Se requiere desarrollar una búsqueda que permita optimizar encontrar un Millennilas y ofertar según su comportamiento de pago y relación de gastos mensuales qué producto bancario se oferta considerando que debe tener capacidad de pago según el % de cada producto bancario.



cambios
